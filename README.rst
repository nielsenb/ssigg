=====
SSIGG
=====

Simple Static Image Gallery Generator
=====================================

SSIGG is a static image gallery generator, written in Python_. It takes
inspiration (and some code) from the static blog generator Pelican_.

Features
========

SSIGG currently supports:

* Processing images (scaling, generating thumbnails) in parallel
* Rendering content templates in parallel
* Themes created with Jinja2_ templates

Limitations
===========

* Currently only JPEGs are supported
* All generated paths are relative
* No albums
* Theme support is currently limited, with only two templates supported
* EXIF parsing is not mature
* Names for template variables are not final
* No tests

Installation
============

SSIGG will not be made available on PyPI_ until it is more stable, it can
still be installed with pip and git::

  $ pip install -e git+https://bitbucket.com/nielsenb/ssigg/ssigg.git#egg=ssigg

Use
===

To generate a spartan image gallery, copy a collection of images to a folder
called ``input``, and simply run::

  $ ssigg

After several seconds (depending on number of images, CPU cores available,
etc.), a simple gallery will appear in the ``output`` folder. Make this gallery
browsable by typing::

  $ cd output
  $ python -m SimpleHTTPServer

And pointing your browser to the path provided.

Settings
========

Control over the output is handled by a settings file, which is a Python
file populated with variable assignments. If a settings file named
``config.py`` is present in the current directory, it will be used, otherwise
a settings file can be given explicitly with the ``-s`` and ``--settings`` options,
for example::

  $ ssigg -s mysettings.py

The currently supported settings are:

**PATH**
  The path for the input files. **DEFAULT:** ``'input'``

**THEME**
  The path to the theme to use when generating the gallery. If not specified,
  the default theme is used.

**OUTPUT_PATH**
  The path to output the generated gallery to, all necessary folders will be
  created. **DEFAULT:** ``'output'``

**COPY_ORIGINALS**
  Whether to copy the original image files to the resulting gallery.
  **DEFAULT:** ``True``

**ORIGINALS_OUTPUT_PATH**
  The path (relative OUTPUT_PATH) where the originals should be copied to.
  **DEFAUlT:** ``'originals'``

**GENERATE_THUMBNAILS**
  Whether thumbnails should be generated. **DEFAULT:** ``True``

**THUMBNAIL_OUTPUT_PATH**
  The path (relative OUTPUT_PATH) where thumbnails should be placed.
  **DEFAULT:** ``'thumbnails'``

**MAX_THUMBNAIL_SIZE**
  The maximum width, and height, of the generated thumbnails. Aspect ratio is
  preserved. **DEFAULT:** ``(320, 200)``

**RESIZE_IMAGES**
  Whether resized images should be generated. **DEFAULT:** ``True``

**RESIZE_OUTPUT_PATH**
  The path (relative OUTPUT_PATH) where resized images should be placed.
  **DEFAULT:** ``'resize'``

**MAX_RESIZE_SIZE**
  The maximum width, and height, of the resized images. Aspect ratio is
  preserved. **DEFAUlT:** ``(1000, 650)``

**PAGINATE**
  Whether the gallery should be paginated. **DEFAULT:** ``False``

**PAGE_SIZE**
  The maximum number of images to appear on a gallery page. **DEFAULT:** ``9``

**USE_TAGS**
  Whether to also tag and organize images by tag, based on TAGS file in
  input folder. If not TAGS file is present, images are placed under the
  'all' tag. **DEFAULT:** ``True``

**DATE_TAGS**
  Whether to also tag and organize images by date. **DEFAULT:** ``True``

**MAX_PROCESSES**
  The number of processes SSIGG is allowed to spawn to process the images.
  Defaults to the number of available CPUs.

Any other desired options can be added to the settings file, and they will be
made available in the theme templates. Settings must be in call capital
letters.

It is also important to note that output paths must be different, or images
will be overwritten, for example, if originals, thumbnails, and resized images
are all being used (the default), ORIGINALS_OUTPUT_PATH, THUMBNAIL_OUTPUT_PATH,
and RESIZE_OUTPUT_PATH must all be different.

Themes
======

Structure
---------

Themes must have the following structure::

    ├── static
    │   ├── css
    │   └── images
    └── templates
        ├── index.html      // to display pages of images (or all images if no pagination) (required)
        ├── tags.html       // to display all tags and allow tag navigation (required)
        ├── dates.html      // to display all date tags and allow date navigation (required)
        └── detail.html     // to display details of a single image (optional)

* ``static`` contains all the static assets, which will be copied
  wholesale, without modification, to the
  folder. The ``css``, and ``images`` folders are suggestions, put whatever you need here.

* ``templates`` contains the templates that will be populated in generating the
  static gallery

Template Variables
------------------

All of the settings are available in the theme templates.

**images**
  List of all available images.

**date_links**
  If **DATE_TAGS** is ``True``, a dictionary with date tags as keys, and links to the date
  tag indices as values.

**tag_links**
  If **USE_TAGS** is ``True``, a dictionary with tags as keys, and links to the tag indices as
  values.

**tagged_images**
  List of images tagged with the current tag.

Image Objects
~~~~~~~~~~~~~

Image objects have the following properties:

**filename**
  The name of the image file, without extension.

**timestamp**
  The time the picture was taken.

**exif**
  A collection of information about the picture, including: ``copyright``,
  ``model`` (the camera model), ``lensmodel``, ``focallength``,
  ``shutterspeed``, ``fstop``, ``iso``.

**original**
  The relative path to the original file, this will not be set if
  **COPY_ORIGINALS** is ``False``.

**thumbnail**
  The relative path to the image thumbnail, this will not be set if
  **GENERATE_THUMBNAILS** is ``False``.

**resize**
  The relative path to a resized version of the image, this will not be set if
  **RESIZE_IMAGES** is ``False``.

**index**
  *index.html only.* The name of the index file the image is shown on, if **PAGINATION** is ``False``,
  this will be ``index.html``.

**index_page**
  *index.html only.* If **PAGINATION** is ``True``, the index page the image appears on.

**prev_detail_link**
  *index.html only.* If the theme being used has a detail template, this will be the name of the
  previous detail file in the image list. Can be used for a slideshow style
  interaction with the detail page.

**next_detail_link**
  *index.html only.* If the theme being used has a detail template, this will be the name of the
  next detail file in the image list. Can be used for a slideshow style
  interaction with the detail page.

**detail**
  If the theme being used has a detail template, this will be the name of the
  detail file for the image.

index.html Specific Variables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**index_link**
  If **PAGINATION** is ``True``, the name of the index file being rendered, eg.
  ``index3.html``, note that the first index page is always ``index.html``.

**page_number**
  If **PAGINATION** is ``True``, the current index page (1 indexed), 1 otherwise.

**total_pages**
  If **PAGINATION** is ``True``, the total number of pages, 1 otherwise.

**image_page**
  If **PAGINATION** is ``True``, list of images on the current index page, otherwise, list of
  all images for the current tag.

**next_index_link**
  If **PAGINATION** is ``True``, the relative link to the next index page.

**prev_index_link**
  If **PAGINATION** is ``True``, the relative link to the previous index page.

detail.html Specific Variables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**image**
  The image object the detail is being rendered for. It has the same properties discussed
  above in the 'Image Objects' section.

**image_number**
  The location of the image within the tagged image list, 1 indexed.

Source
======

Source is available at: https://bitbucket.org/nielsenb/ssigg

.. _Python: http://www.python.org/
.. _Pelican: http://docs.getpelican.com
.. _Jinja2: http://jinja.pocoo.org/
.. _PyPI: https://pypi.python.org/pypi
