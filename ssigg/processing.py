#Copyright (C) 2013  Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import sys
import os

from functools import partial
from multiprocessing import Pool, cpu_count
from PIL import Image, ExifTags

PY2 = sys.version_info[0] == 2

if PY2:
    range = xrange
else:
    range = range

def create_thumbnails(inputfolderlist, inputfilelist, outputdir, maxwidth, maxheight, processes=cpu_count()):
    #Create the thumbnails, in parallel
    inputtuples = []

    for inputindex in range(0, len(inputfolderlist)):
        inputtuples.append((inputfolderlist[inputindex], inputfilelist[inputindex]))

    thumbnailpool = Pool(processes=processes)
    thumbnailpartial = partial(_create_thumbnail, outputdir=outputdir, maxwidth=maxwidth, maxheight=maxheight)
    thumbnailpool.map(thumbnailpartial, inputtuples)
    thumbnailpool.close()
    thumbnailpool.join()

def _create_thumbnail(inputtuple, outputdir, maxwidth, maxheight):
    #Given a tuple of (input folder, input file path), create a thumbnail with the same
    #filename in thumbnailoutputpath, with the maximum size of maxwidth x maxheight
    try:
        inputfolder = inputtuple[0]
        inputfilepath = inputtuple[1]

        image = Image.open(inputfilepath)

        #Determine if we need to rotate
        #https://stackoverflow.com/questions/4228530/pil-thumbnail-is-rotating-my-image
        rawexifdict = _get_exif_tags(image)

        orientation = _clean_exif_tag(rawexifdict, ['Orientation'])

        if orientation == 3:
            image = image.rotate(180, expand=True)
        elif orientation == 6:
            image = image.rotate(270, expand=True)
        elif orientation == 8:
            image = image.rotate(90, expand=True)

        image.thumbnail((maxwidth, maxheight), Image.ANTIALIAS) #Antialiasing produces a better result
        image.save(os.path.join(outputdir, inputfolder, os.path.basename(inputfilepath)), 'JPEG')
    except IOError:
        raise IOError('Cannot create thumbnail for {inputfile}'.format(inputfile=inputfilepath))

def build_image_info_list(inputfolderlist, inputfilelist, inputtaglist, settings):
    #We use the settings dictionary to make it easy to access what we need
    imageinfolist = []

    for inputindex in range(0, len(inputfolderlist)):
        inputfolder = inputfolderlist[inputindex]
        inputfile = inputfilelist[inputindex]

        if settings['USE_TAGS'] is True:
            inputtags = inputtaglist[inputindex]
        else:
            inputtags = None

        imagefilename = os.path.basename(inputfile)

        #These paths are relative the output directory
        if settings['COPY_ORIGINALS'] is True:
            originalfilepath = os.path.join(settings['ORIGINAL_OUTPUT_PATH'], inputfolder, imagefilename)
        else:
            originalfilepath = None

        if settings['GENERATE_THUMBNAILS'] is True:
            thumbnailfilepath = os.path.join(settings['THUMBNAIL_OUTPUT_PATH'], inputfolder, imagefilename)
        else:
            thumbnailfilepath = None

        if settings['RESIZE_IMAGES'] is True:
            resizefilepath = os.path.join(settings['RESIZE_OUTPUT_PATH'], inputfolder, imagefilename)
        else:
            resizefilepath = None

        if settings['GENERATE_THUMBNAILS'] is True:
            #Make sure there is a thumbnail for each input file
            if os.path.isfile(os.path.join(settings['OUTPUT_PATH'], thumbnailfilepath)) is False:
                raise IOError('No matching thumbnail for {inputfile}'.format(inputfile=inputfile))

        #Extract the EXIF data
        rawexifdict = _get_exif_tags(Image.open(inputfile))

        #Clean the dict up a little bit
        exifdict = {}

        if _clean_exif_tag(rawexifdict, ['Copyright']) is not None:
            exifdict['copyright'] = _clean_exif_tag(rawexifdict, ['Copyright'])

        if _clean_exif_tag(rawexifdict, ['Model']) is not None:
            exifdict['model'] = _clean_exif_tag(rawexifdict, ['Model'])

        if _clean_exif_tag(rawexifdict, ['LensModel']) is not None:
            exifdict['lensmodel'] = _clean_exif_tag(rawexifdict, ['LensModel'])

        if _clean_exif_tag(rawexifdict, ['FocalLengthIn35mmFilm']) is not None:
            exifdict['focallength'] = '{focallength}mm'.format(focallength=_clean_exif_tag(rawexifdict, ['FocalLengthIn35mmFilm']))

        if _clean_exif_tag(rawexifdict, ['ExposureTime']) is not None:
            shutterspeedtuple = _clean_exif_tag(rawexifdict, ['ExposureTime'])
            exifdict['shutterspeed'] = str(shutterspeedtuple[0]) + '/' + str(shutterspeedtuple[1]) + ' sec.'

        if _clean_exif_tag(rawexifdict, ['FNumber', 'ApertureValue']) is not None:
            #Calculate the fstop
            fstoptuple = _clean_exif_tag(rawexifdict, ['FNumber', 'ApertureValue'])
            fstop = float(fstoptuple[0]) / float(fstoptuple[1])

            exifdict['fstop'] = fstop

        if _clean_exif_tag(rawexifdict, ['ISOSpeedRatings']) is not None:
            exifdict['iso'] = _clean_exif_tag(rawexifdict, ['ISOSpeedRatings'])

        #Try to figure out a timestamp for the image
        if _clean_exif_tag(rawexifdict, ['DateTimeOriginal', 'DateTimeDigitized', 'DateTime']) is not None:
            #Best case, we can build a timestamp from the EXIF
            timestamp = datetime.datetime.strptime(_clean_exif_tag(rawexifdict, ['DateTimeOriginal', 'DateTimeDigitized', 'DateTime']), '%Y:%m:%d %H:%M:%S')
        else:
            #Build the timestamp from the file creation time (basically worthless, since its a copy, but better
            #than nothing)
            timestamp = datetime.datetime.fromtimestamp(os.path.getmtime(inputfile))

        #Now, we can create and append the new info object
        imageinfolist.append(_build_info_dict(filename=os.path.splitext(imagefilename)[0], filefolder=inputfolder, originalfilepath=originalfilepath, thumbnailfilepath=thumbnailfilepath, resizefilepath=resizefilepath, timestamp=timestamp, exifdict=exifdict, tags=inputtags))

    return imageinfolist

def _get_exif_tags(image):
    #Convert the EXIF data into a useful dict
    #http://stackoverflow.com/questions/4764932/in-python-how-do-i-read-the-exif-data-for-an-image
    rawexifdict = {
        ExifTags.TAGS[k]: v
        for k, v in image._getexif().items()
        if k in ExifTags.TAGS
    }

    return rawexifdict

def _clean_exif_tag(rawexifdict, rawtaglist):
    #Given the dict of raw exif data, a list of potential names the desired
    #EXIF data would appear under, returns the first of the requested value
    #or None if not found
    for tag in rawtaglist:
        if tag in rawexifdict:
            return rawexifdict[tag]

    return None

def _build_info_dict(filename, filefolder, originalfilepath, thumbnailfilepath, resizefilepath, timestamp, exifdict, tags):
    infodict = {'filename': filename, 'filefolder': filefolder, 'timestamp': timestamp, 'exif': exifdict}

    if originalfilepath is not None:
        infodict['original'] = '/' + originalfilepath

    if thumbnailfilepath is not None:
        infodict['thumbnail'] = '/' +thumbnailfilepath

    if resizefilepath is not None:
        infodict['resize'] = '/' + resizefilepath

    if tags is not None:
        infodict['tags'] = tags

    return infodict
