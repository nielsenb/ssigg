#Copyright (C) 2013  Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy
import os
import sys
import urllib
import distutils.dir_util

from functools import partial
from jinja2 import Environment, FileSystemLoader
from multiprocessing import Pool, cpu_count
from ssigg.util import make_dir, n_split

PY2 = sys.version_info[0] == 2

if PY2:
    quote_plus = urllib.quote_plus
else:
    quote_plus = urllib.parse.quote_plus

def render_content(imageinfodicts, settings, processes=cpu_count()):
    #Render everything necessary to do a gallery using the given info dicts
    #Whether or not to render detail templates
    renderdetail = os.path.exists(os.path.join(settings['THEME'], 'templates', 'detail.html'))

    #Split infodicts up by date, and again by tag
    imageinfodictsbydate = {}
    imageinfodictsbytag = {}

    if settings['DATE_TAGS'] is True or settings['USE_TAGS'] is True:
        for imageinfodict in imageinfodicts:
            if settings['DATE_TAGS'] is True:
                imagedate = imageinfodict['timestamp'].date()

                if imagedate.isoformat() not in imageinfodictsbydate:
                    imageinfodictsbydate[imagedate.isoformat()] = []

                imageinfodictsbydate[imagedate.isoformat()].append(imageinfodict)

            if settings['USE_TAGS'] is True:
                for tag in imageinfodict['tags']:
                    if tag not in imageinfodictsbytag:
                        imageinfodictsbytag[tag] = []

                    imageinfodictsbytag[tag].append(imageinfodict)

    #Build content common to all templates
    content = settings.copy()

    content['images'] = imageinfodicts

    if settings['DATE_TAGS'] is True:
        datelinks = {}

        for unquoteddate in imageinfodictsbydate.keys():
            datelinks[unquoteddate] = '/' + quote_plus(unquoteddate)

        content['date_links'] = datelinks

    if settings['USE_TAGS'] is True:
        taglinks = {}

        for unquotedtag in imageinfodictsbytag.keys():
            taglinks[unquotedtag] = '/' + quote_plus(unquotedtag)

        content['tag_links'] = taglinks

    imageinfodictsforall = _process_info_dicts(imageinfodicts, None, renderdetail, settings)

    tagtuples = [(imageinfodictsforall, None)] #None is the 'all' tag
    detaildicts = []

    if renderdetail is True:
        detaildicts.append(imageinfodictsforall)

    if settings['DATE_TAGS'] is True:
        #Build date tags
        for datetag in imageinfodictsbydate:
            imageinfodictsfordate = _process_info_dicts(imageinfodictsbydate[datetag], datetag, renderdetail, settings)

            tagtuples.append((imageinfodictsfordate, datetag))

            if renderdetail is True:
                detaildicts.append(imageinfodictsfordate)

        #Render date navigation
        _process_nav_template('DATES', content, settings)

    if settings['USE_TAGS'] is True:
        #Build content tags
        for tag in imageinfodictsbytag:
            imageinfodictsfortag = _process_info_dicts(imageinfodictsbytag[tag], tag, renderdetail, settings)

            tagtuples.append((imageinfodictsfortag, tag))

            if renderdetail is True:
                detaildicts.append(imageinfodictsfortag)

        #Render tag navigation
        _process_nav_template('TAGS', content, settings)

    #Render content in parallel
    renderpool = Pool(processes=processes)

    #Index jobs
    indexpartial = partial(_render_tag_indices, content=content, settings=settings)
    renderpool.map(indexpartial, tagtuples)

    #Detail jobs
    detailpartial = partial(_render_tag_details, content=content, settings=settings)
    renderpool.map(detailpartial, detaildicts)

    #Render
    renderpool.close()
    renderpool.join()

    #Now copy the static dir from the theme wholesale into the output (overwrite if already exists)
    distutils.dir_util.copy_tree(os.path.join(settings['THEME'], 'static'), os.path.join(settings['OUTPUT_PATH'], 'static'))

def _process_info_dicts(imageinfodicts, tag, renderdetail, settings):
    #Adds index and detail navigation info to imageinfodicts
    processedimageinfodicts = copy.deepcopy(imageinfodicts)

    if tag is None:
        #When no tag is given, images are stored under the 'all' tag
        tag = 'all'

    quotedtag = quote_plus(tag)

    #Add some information to each image
    for imageindex, imageinfodict in enumerate(processedimageinfodicts):
        if settings['PAGINATE'] is False:
            #Handle every image having the same index, paginated case is handled later
            imageinfodict['index_link'] = 'index.html'
            imageinfodict['index_page'] = 1
        else:
            #There will be multiple indexes, one per page, the first page
            #will just be index, next is index2, etc., etc.
            pagenumber = int(imageindex / settings['PAGE_SIZE']) + 1 #Pages are 1 indexed

            #Figure out the index file name, index1 is just index
            if pagenumber == 1:
                indexfilename = 'index.html'
            else:
                indexfilename = 'index' + str(pagenumber) + '.html'

            #Attach the index each image appears on
            if tag == 'all':
                imageinfodict['index_link'] = '/' + indexfilename
            else:
                imageinfodict['index_link'] = '/' + '/'.join([quotedtag, indexfilename])

            imageinfodict['index_page'] = pagenumber

        if renderdetail is True:
            if imageindex > 0:
                previnfodict = processedimageinfodicts[imageindex - 1]

                #Set the link to the previous image detail
                imageinfodict['prev_detail_link'] = '/' + '/'.join([quotedtag, previnfodict['filefolder'], previnfodict['filename']]) + '.html'

            if imageindex < len(processedimageinfodicts) - 1:
                nextinfodict = processedimageinfodicts[imageindex + 1]

                #Set the link to the next image detail
                imageinfodict['next_detail_link'] = '/' + '/'.join([quotedtag, nextinfodict['filefolder'], nextinfodict['filename']]) + '.html'

            #Set the link to the current image detail
            imageinfodict['detail_link'] = '/' + '/'.join([quotedtag, imageinfodict['filefolder'], imageinfodict['filename']]) + '.html'

    return processedimageinfodicts

def _render_tag_indices(imageinfotuple, content, settings):
    jinjaenvironment = Environment(loader=FileSystemLoader(os.path.join(settings['THEME'], 'templates')))
    indextemplate = jinjaenvironment.get_template('index.html')

    imageinfodicts = imageinfotuple[0]
    tag = imageinfotuple[1]

    if tag is None:
        #When no tag is given, images are stored under the 'all' tag
        tag = 'all'

    quotedtag = quote_plus(tag)

    #Do no damage
    tagcontent = content.copy()

    #Tag is common to all templates
    tagcontent['tag'] = tag
    tagcontent['tagged_images'] = imageinfodicts

    #Set up page content
    if settings['PAGINATE'] is True:
        paginatedimageinfodicts = n_split(imageinfodicts, settings['PAGE_SIZE'])
        totalpages = len(paginatedimageinfodicts)

        for pageindex, imageinfodictpage in enumerate(paginatedimageinfodicts):
            #Start fresh for each loop
            indexcontent = tagcontent.copy()

            pagenumber = pageindex + 1 #Index is 0 indexed, pages are 1 indexed

            #Figure out the index file name, index1 is just index
            if pagenumber == 1:
                indexfilename = 'index.html'
            else:
                indexfilename = 'index' + str(pagenumber) + '.html'

            indexcontent['image_page'] = imageinfodictpage
            indexcontent['page_number'] = pagenumber
            indexcontent['total_pages'] = totalpages

            if pagenumber < totalpages:
                #Set link to index
                if tag == 'all':
                    indexcontent['next_index_link'] = '/' + 'index' + str(pagenumber + 1) + '.html'
                else:
                    indexcontent['next_index_link'] = '/' + '/'.join([quotedtag, 'index' + str(pagenumber + 1) + '.html'])

            if pagenumber > 1:
                #index1 is just index
                if pagenumber - 1 == 1:
                    if tag == 'all':
                        indexcontent['prev_index_link'] = '/' + 'index.html'
                    else:
                        indexcontent['prev_index_link'] = '/' + '/'.join([quotedtag, 'index.html'])
                else:
                    if tag == 'all':
                        indexcontent['prev_index_link'] = '/' + 'index' + str(pagenumber - 1) + '.html'
                    else:
                        indexcontent['prev_index_link'] = '/' + '/'.join([quotedtag, 'index' + str(pagenumber - 1) + '.html'])

            renderedtemplate = indextemplate.render(indexcontent)

            #The 'all' tag is also our root index
            if tag == 'all':
                _write_file(renderedtemplate, os.path.join(settings['OUTPUT_PATH'], indexfilename))
            else:
                #Make the folder for the tag output
                make_dir(os.path.join(settings['OUTPUT_PATH'], quotedtag))

                _write_file(renderedtemplate, os.path.join(settings['OUTPUT_PATH'], quotedtag, indexfilename))
    else:
        tagcontent['page_number'] = 1
        tagcontent['total_pages'] = 1
        tagcontent['image_page'] = imageinfodicts

        renderedtemplate = indextemplate.render(tagcontent)

        #The 'all' tag is our root index
        if tag == 'all':
            _write_file(renderedtemplate, os.path.join(settings['OUTPUT_PATH'], 'index.html'))
        else:
            #Make the folder for the tag output
            make_dir(os.path.join(settings['OUTPUT_PATH'], quotedtag))

            _write_file(renderedtemplate, os.path.join(settings['OUTPUT_PATH'], quotedtag, 'index.html'))

def _render_tag_details(imageinfodicts, content, settings):
    jinjaenvironment = Environment(loader=FileSystemLoader(os.path.join(settings['THEME'], 'templates')))
    detailtemplate = jinjaenvironment.get_template('detail.html')

    for imageindex, imageinfodict in enumerate(imageinfodicts):
        #Start fresh for each loop
        detailcontent = content.copy()

        detailcontent['tagged_images'] = imageinfodicts

        detailcontent['image'] = imageinfodict
        detailcontent['image_number'] = imageindex + 1 #The number should be 1 indexed

        renderedtemplate = detailtemplate.render(detailcontent)

        #Make output folder (strip leading '/' making absolute URL relative to the filesystem)
        make_dir(os.path.join(settings['OUTPUT_PATH'], os.path.dirname(imageinfodict['detail_link'][1:])))

        _write_file(renderedtemplate, os.path.join(settings['OUTPUT_PATH'], imageinfodict['detail_link'][1:]))

def _process_nav_template(templatetype, content, settings):
    if templatetype == 'TAGS':
        templatename = 'tags.html'
    elif templatetype == 'DATES':
        templatename = 'dates.html'
    else:
        raise RuntimeError('Invalid nav template.')

    jinjaenvironment = Environment(loader=FileSystemLoader(os.path.join(settings['THEME'], 'templates')))
    template = jinjaenvironment.get_template(templatename)
    renderedtemplate = template.render(content)

    _write_file(renderedtemplate, os.path.join(settings['OUTPUT_PATH'], templatename))

def _write_file(content, filepath):
    #Writes to a file, in UTF-8
    with open(filepath, 'wb') as towrite:
        towrite.write(content.encode('utf8'))
