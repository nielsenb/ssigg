#Copyright (C) 2013  Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

def get_input_file_list(inputfolderpath):
    #Given an input folder, returns a tuple of the form
    #(input folder prefixes, input file paths, file tags)
    folderlist = []
    filelist = []
    taglist = []

    for root, _, files in os.walk(inputfolderpath):
        for inputfile in files:
            if os.path.basename(inputfile) != 'TAGS':
                inputpath = os.path.relpath(root, inputfolderpath)

                folderlist.append(inputpath)
                filelist.append(os.path.join(root, inputfile))
                taglist.append(_process_taglist(root))

    return (folderlist, filelist, taglist)

def _process_taglist(inputpath):
    #Given a path, reads a TAGS file and returns a list
    #of tags
    taglist = []

    tagfile = os.path.join(inputpath, 'TAGS')

    if os.path.isfile(tagfile):
        with open(tagfile, 'r') as infile:
            tagcontent = infile.readlines()

            for tagline in tagcontent:
                for tag in tagline.split(','):
                    taglist.append(tag.lower().strip())

    return taglist
