#Copyright (C) 2013  Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil

def make_dir(pathtomake):
    #Given a directory path to make, makes the necessary directory tree,
    #if the directory already exists, does nothing
    if os.path.exists(pathtomake) is False:
        os.makedirs(pathtomake)

def copy_dir(srcpath, destpath):
    #Copies the files in the given src, to the given destination, while preserving
    #timestamps
    for srcfilename in os.listdir(srcpath):
        srcfilepath = os.path.join(srcpath, srcfilename)

        if os.path.isfile(srcfilepath) is True:
            shutil.copy(srcfilepath, os.path.join(destpath, srcfilename))

            #Try to preserve modification time
            shutil.copystat(srcfilepath, os.path.join(destpath, srcfilename))

def n_split(iterable, n):
    #Split an iterable in to chunks of at most size n
    #http://stackoverflow.com/questions/2233204/how-does-zipitersn-work-in-python
    num_extra = len(iterable) % n
    zipped = list(zip(*[iter(iterable)] * n))

    return zipped if not num_extra else zipped + [iterable[-num_extra:], ]
