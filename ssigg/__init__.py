#Copyright (C) 2013  Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import operator
import os.path

from multiprocessing import cpu_count
from ssigg.settings import read_settings
from ssigg.preprocessing import get_input_file_list
from ssigg.processing import create_thumbnails, build_image_info_list
from ssigg.rendering import render_content
from ssigg.util import make_dir, copy_dir

def main():
    #Read any config options
    args = parse_arguments()

    config_file = args.settings

    if config_file is None and os.path.isfile('config.py'):
        config_file = 'config.py'

    settings = read_settings(config_file, override=get_config(args))

    #Create the output directory
    make_dir(settings['OUTPUT_PATH'])

    #Build the list of images to process
    inputfolderlist, inputfilelist, inputtaglist = get_input_file_list(settings['PATH'])

    #Create the thumbnails, in parallel
    if settings['GENERATE_THUMBNAILS'] is True:
        thumbnailfolder = os.path.join(settings['OUTPUT_PATH'], settings['THUMBNAIL_OUTPUT_PATH'])

        #Create any required folders
        for inputfolder in inputfolderlist:
            make_dir(os.path.join(thumbnailfolder, inputfolder))

        create_thumbnails(inputfolderlist, inputfilelist, thumbnailfolder, settings['MAX_THUMBNAIL_SIZE'][0], settings['MAX_THUMBNAIL_SIZE'][1], processes=settings['MAX_PROCESSES'])

    #Create resized images, in parallel
    if settings['RESIZE_IMAGES'] is True:
        scaledfolder = os.path.join(settings['OUTPUT_PATH'], settings['RESIZE_OUTPUT_PATH'])

        #Create any required folders
        for inputfolder in inputfolderlist:
            make_dir(os.path.join(scaledfolder, inputfolder))

        #Note that the thumbnail method we use is just so aspect ratio is preserved, it isn't
        #just for thumbnail sized images
        create_thumbnails(inputfolderlist, inputfilelist, scaledfolder, settings['MAX_RESIZE_SIZE'][0], settings['MAX_RESIZE_SIZE'][1], processes=settings['MAX_PROCESSES'])

    #Copy the original files
    if settings['COPY_ORIGINALS'] is True:
        originalfolder = os.path.join(settings['OUTPUT_PATH'], settings['ORIGINAL_OUTPUT_PATH'])

        #In this case, dedupe so we don't copy multiple times
        for inputfolder in list(set(inputfolderlist)):
            make_dir(os.path.join(originalfolder, inputfolder))
            copy_dir(os.path.join(settings['PATH'], inputfolder), os.path.join(originalfolder, inputfolder))

    #Build info dicts for all images
    imageinfodicts = build_image_info_list(inputfolderlist, inputfilelist, inputtaglist, settings)

    #Sort the dicts by timestamp
    imageinfodicts.sort(key=operator.itemgetter('timestamp'), reverse=True)

    #Now render the output
    render_content(imageinfodicts, settings, processes=settings['MAX_PROCESSES'])

def parse_arguments():
    #Shamelessly stolen from Pelican (https://github.com/getpelican/pelican/blob/master/pelican/__init__.py)
    parser = argparse.ArgumentParser(description='A tool to generate a static image gallery from a folder of images', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(dest='path', nargs='?', help='Path where to find the source images.')

    parser.add_argument('-t', '--theme-path', dest='theme', help='Path where to find the theme templates. If not specified, a spartan default theme will be used')

    parser.add_argument('-o', '--output', dest='output', help='Where to output the generated files. If not specified, a directory will be created, named "output" in the current path.')

    parser.add_argument('-p', '--processes', dest='processes', help='Maximum number of processes to spawn. Defaults to the number of available CPUs.', default=cpu_count())

    parser.add_argument('-s', '--settings', dest='settings', help='The settings of the application, this is automatically set to {0} if a file exists with this name.'.format('config.py'))

    return parser.parse_args()

def get_config(args):
    #Build config dictionary from args
    #Shamelessly stolen from Pelican (https://github.com/getpelican/pelican/blob/master/pelican/__init__.py)
    config = {}

    if args.path is not None:
        config['PATH'] = os.path.abspath(os.path.expanduser(args.path))

    if args.output is not None:
        config['OUTPUT_PATH'] = os.path.abspath(os.path.expanduser(args.output))

    if args.processes is not None:
        config['MAX_PROCESSES'] = int(args.processes)

    if args.theme is not None:
        abstheme = os.path.abspath(os.path.expanduser(args.theme))

        if os.path.exists(abstheme):
            config['THEME'] = abstheme
        else:
            config['THEME'] = args.theme
