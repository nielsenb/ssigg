#Copyright (C) 2013  Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import sys

PORT = 8000
PY2 = sys.version_info[0] == 2

if PY2:
    import SimpleHTTPServer
    import SocketServer

    handler = SimpleHTTPServer.SimpleHTTPRequestHandler

    httpd = SocketServer.TCPServer(('', PORT), handler)

    print('serving at', 'http://localhost:' + str(PORT))
    httpd.serve_forever()
else:
    import http.server
    import socketserver

    handler = http.server.SimpleHTTPRequestHandler

    with socketserver.TCPServer(('', PORT), handler) as httpd:
        print('serving at', 'http://localhost:' + str(PORT))
        httpd.serve_forever()
