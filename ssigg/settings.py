#Copyright (C) 2013  Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import copy
import inspect

from imp import load_source
from multiprocessing import cpu_count
from os.path import isabs

DEFAULT_THEME = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'theme', 'default')

DEFAULT_CONFIG = {
    'PATH': 'input',
    'THEME': DEFAULT_THEME,
    'OUTPUT_PATH': 'output',
    'COPY_ORIGINALS': True,
    'ORIGINAL_OUTPUT_PATH': 'originals',
    'GENERATE_THUMBNAILS': True,
    'THUMBNAIL_OUTPUT_PATH': 'thumbnails',
    'MAX_THUMBNAIL_SIZE': (320, 200), #Chosen to look best with default theme
    'RESIZE_IMAGES': True, #Used for a 'medium' sized image
    'RESIZE_OUTPUT_PATH': 'resize',
    'MAX_RESIZE_SIZE': (1000, 650),
    'PAGINATE': False, #Chosen to look best with default theme
    'PAGE_SIZE': 9, #Chosen to look best with default theme
    'USE_TAGS': True,
    'DATE_TAGS': True,
    'MAX_PROCESSES': cpu_count(),
    }

def read_settings(path=None, override=None):
    #Shamelessly stolen from Pelican (https://github.com/getpelican/pelican/blob/master/pelican/settings.py)
    if path:
        local_settings = get_settings_from_file(path)
        # Make the paths relative to the settings file
        for p in ['PATH', 'OUTPUT_PATH', 'THEME']:
            if p in local_settings and local_settings[p] is not None and not isabs(local_settings[p]):
                absp = os.path.abspath(os.path.normpath(os.path.join(os.path.dirname(path), local_settings[p])))

                if p not in ('THEME') or os.path.exists(absp):
                    local_settings[p] = absp
    else:
        local_settings = copy.deepcopy(DEFAULT_CONFIG)

    if override:
        local_settings.update(override)

    check_settings(local_settings)

    return local_settings

def get_settings_from_module(module=None, default_settings=DEFAULT_CONFIG):
    """Loads settings from a module, returns a dictionary."""

    #Shamelessly stolen from Pelican (https://github.com/getpelican/pelican/blob/master/pelican/settings.py)
    context = copy.deepcopy(default_settings)
    if module is not None:
        context.update((k, v) for k, v in inspect.getmembers(module) if k.isupper())

    return context

def get_settings_from_file(path, default_settings=DEFAULT_CONFIG):
    """Loads settings from a file path, returning a dict."""

    #Shamelessly stolen from Pelican (https://github.com/getpelican/pelican/blob/master/pelican/settings.py)
    name, ext = os.path.splitext(os.path.basename(path))
    module = load_source(name, path)

    return get_settings_from_module(module, default_settings=default_settings)

def check_settings(settings):
    """Provide error checking and warnings for the given settings.

    """

    #Shamelessly stolen from Pelican (https://github.com/getpelican/pelican/blob/master/pelican/settings.py)
    if not 'PATH' in settings or not os.path.isdir(settings['PATH']):
        raise Exception('You need to specify a path containing the content')

    # lookup the theme if the given one doesn't exist
    if not os.path.isdir(settings['THEME']):
        raise Exception('Could not find the theme {0}'.format(settings['THEME']))

    return settings
