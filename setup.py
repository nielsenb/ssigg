from setuptools import setup, find_packages

entry_points = {
    'console_scripts': [
        'ssigg = ssigg:main',
    ]
}

setup(name = 'ssigg',
      version = '0.23.0',
      entry_points=entry_points,
      packages = find_packages(),
      include_package_data = True,
      install_requires = [
        'Pillow>=4.2.1, <5.0.0',
        'jinja2>=2.9.6, <3.0.0'
      ],
)
